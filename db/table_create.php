<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "np";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// sql to create table
$sql_0 = "CREATE TABLE cities (
ID int NOT NULL AUTO_INCREMENT,
DescriptionRu VARCHAR(255) NOT NULL,
Ref VARCHAR(255) NOT NULL,
PRIMARY KEY (ID));
";

$sql_1 = "CREATE TABLE warehouses (
ID int NOT NULL AUTO_INCREMENT,
DescriptionRu VARCHAR(255) NOT NULL,
CityRef VARCHAR(255) NOT NULL,
Longitude VARCHAR(255),
Latitude VARCHAR(255),
PRIMARY KEY (ID)
);";

if ($conn->query($sql_0) === TRUE) {
    echo "Table  created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}
if ($conn->query($sql_1) === TRUE) {
    echo "Table  created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

        
$conn->close();
?>




