<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Marker Labels</title>
    <!-- this style need to be inside of this file for google api -->
    <style type="text/css" >
        html, body {
        height: 395px;
        width:545px;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHTh6nSg2gRFHylGxxS8JackL8ia-Wo34&signed_in=true"></script>
    <script>//need to be inside of this file
    //googla api adapted copypaste
    var map;
    var markers = [];

    function initialize() {
      var kiev = { lat: 50.2700, lng: 30.3125 };
      window.map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5,
        center: kiev
      });

    }

    // Adds a marker to the map.
    function addMarker(location, map,description) {
        console.log(description);
      // Add the marker at the clicked location, and add the next-available label
      // from the array of alphabetical characters.
      var marker = new google.maps.Marker({
        position: location,
        label: description,
        map: map
      });
      markers.push(marker);
    }

    function deleteMarkers() {
      setMapOnAll(null);
    }

    function setMapOnAll(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }
    }

    google.maps.event.addDomListener(window, 'load', initialize);
        function init() { window.parent.setUpFrame(); return true; }
        function handler(a,b) {
            addMarker(b,window.map,a);
        }
    </script>
  </head>
  <body onload="init();">
    <div id="map"></div>
  </body>
</html>
