<?php	
   include('./layouts/header.php');
      include('./db/db_connection.php');
      ?>
<?php 
   $sql = "SELECT * FROM cities ORDER BY ID;";
   $result = $conn->query($sql);
   $rows = array();
   
   if ($result->num_rows > 0) {
       // output data of each row
       while($row = $result->fetch_assoc()) {
           $rows[] = $row;
       }
       
   } else {
       echo "0 results";
   }
   $cities = json_encode($rows);
   $conn->close();
   ?>
<div ng-app="myApp" ng-controller="myCtrl">
    <div class="form-group">
        <label>Search</label>
        <input type="text" ng-model="name" class="form-control">
   </div>
   <table class='table table-bordered'>
      <thead>
         <tr>
            <th class="text-center">ID</th>
            <th class="text-center">Description</th>
            <th  hidden>ref</th>
            <th class="text-center">button</th>
         </tr>
      </thead>
      <tbody>
         <tr ng-repeat="city in cities|filter:name">
            <td>{{ city.ID }}</td>
            <td class="text-center">{{ city.DescriptionRu }}</td>
            <td hidden>{{ city.Ref }}</td>
            <td class="text-center">
               <button type="button" class="btn btn-primary" 
                  data-toggle="modal" data-target="#exampleModal" 
                  data-ref="{{ city.Ref }}">
               Show warehouses</button>
            </td>
         </tr>
      </tbody>
   </table>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="exampleModalLabel">Map</h4>
   </div>
   <div class="modal-body" id='map' style="min-height:400px;overflow-y: auto;">
      <div ng-app="myApp" ng-controller="myCtrl">
         <table class='table table-bordered'>
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Description</th>
               </tr>
            </thead>
            <tbody id="mbody" ></tbody>
         </table>
         <div>
            <iframe  name="frame-id" src="mapstest.php" height="400px" width="550px"></iframe>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="del()">
            Close</button>
            <button type="button" class="btn btn-primary">Some action</button>
         </div>
      </div>
   </div>
</div>
<script src="static/js/angularCtrl.js"></script>
<script>
   var cities = <?php echo $cities;?>;
       //google api
   function setUpFrame() { 
       var frame = window.frames['frame-id'];
       console.log('loaded');
   }
   function del() {
       var frame = window.frames['frame-id'];
       frame.deleteMarkers();
       $('#mbody').empty();
   }
</script>
<?php	
   include('./layouts/footer.php');
      ?>