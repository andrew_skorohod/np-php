$('#exampleModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var ref = button.data('ref'); // Extract info from data-* attributes
    console.log(ref);
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    $.ajax({
        data: {
            'ref': ref
        },
        url: 'show_warehouse.php',
        type: 'GET',
        success: function(response) {
            var response = JSON.parse(response);
            for (i = 0; i < response.length; i++) {

                var id = response[i]['ID'];
                var description = response[i]['DescriptionRu'];
                var location = {
                    lat: parseFloat(response[i]['Latitude']),
                    lng: parseFloat(response[i]['Longitude'])
                };


                frame = window.frames['frame-id'];
                frame.handler(description, location);
                $('#mbody').append('<tr><td>' + id + '</td><td>' + description + '</td></tr>');
            }


        }
    });

});

